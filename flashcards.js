let lists = {
	next: ['ArrowUp','ArrowLeft','ArrowRight','ArrowDown','Enter','h','j','k','l','H','J','K','L',' ','n','N'],
	del: ['d','D'],
	restart: ['r', 'R'],
	save:['s','S']};

let data = {
	reset: "",
	current: "",
	cur_key: "",
};


let app = new Vue( {
	el: '#app',
	data: {
		question: 'Drag over a CSV or JSON file to get started!',
		answer: '',
		link: '',
		notstarted: true,
		length: 0,
		dontshowanswer: true 
	},
	created() {
		window.addEventListener('keydown', function(event) {
			if(app.notstarted) { 
				return;
			}
			if(lists.next.indexOf(event.key) != -1){
				data.cur_key = next(data.current, data.cur_key);	
			} else if(lists.del.indexOf(event.key) != -1) {
				delete data.current[data.cur_key];
				update_save(data.current);
				app.length--;
				app.dontshowanswer = false;
				data.cur_key = next(data.current, data.cur_key);
			} else if(lists.save.indexOf(event.key) != -1) {
				update_save(data.current);	

			} else if(lists.restart.indexOf(event.key) != -1) {
				data.current = JSON.parse(JSON.stringify(data.reset));
				app.length = Object.keys(data.current).length;
				data.cur_key = Object.keys(data.current)[0];
				app.dontshowanswer = false;
				data.cur_key = next(data.current, data.cur_key);
			}else {
			}
		});
	}
} );

app.length = Object.keys(data.current).length;

function randi(n) {
	return Math.floor(Math.random() * n);
}

function rand_entry(mydata, mykey) {
	let keys = Object.keys(mydata);
	let index = randi(keys.length);
	if( keys[index] == mykey ) {
		index = (index + 1) % keys.length;
	}
	return {'key': keys[index], 'entry':mydata[keys[index]]};
}

function update_save(obj_data) {
	let blob = new Blob([JSON.stringify(obj_data)], {type : 'application/json'});
	app.link = URL.createObjectURL(blob);
}

function load(text, format) {
	if(format == "csv") {
		app.notstarted = false;
		let data_tmp = text.split('\n');
		// filter empty lines and commented lines
		data_tmp = data_tmp.filter( t => { 
			if( t == "" ) {
				return 0;
			}
			return (t[0] != '#');
		});
		data.current = {...data_tmp.map( function (csvstr) {
			let tmp = csvstr.search(';'); 
			return {'question':csvstr.substring(0,tmp), 'answer':csvstr.substring(tmp + 1)}; 
		}
		)};
	} else if(format == "json") {
		app.notstarted = false;
		data.current = JSON.parse(text);
	} else {
	}

	app.length = Object.keys(data.current).length;
	data.reset = JSON.parse(JSON.stringify(data.current));
	update_save(data.current);
	data.cur_key = Object.keys(data.current)[0]
	app.dontshowanswer = false;
	data.cur_key = next(data.current, data.cur_key);

}

function next(mydata, mykey) {
	if(app.length == 0) {
		app.question =  "Done with stack!"
		app.answer = "Press R to restart.";
		return "";
	}
	if(app.dontshowanswer) {
		app.dontshowanswer = false;
		return mykey;
	}
	app.dontshowanswer = true;
	tmp = rand_entry(mydata, mykey);
	app.question = tmp.entry.question;
	app.answer = tmp.entry.answer; 
	return tmp.key;
}

function dropHandler(event) {
	event.preventDefault();
}


let div = document.getElementById('app');
div.style = "border: 6px dotted white;";
div.ondragenter = function (e) { 
	this.className = 'nicenice lvl-over'; 
	div.style = "border: 6px dotted blue;"
	return false; 
};
div.ondragexit = function () { 
	div.style = "border: 6px dotted white;";
};
div.ondrop = function (e) {
	e.preventDefault();
	div.style = "border: 6px dotted white;";
	let filename = e.dataTransfer.files[0].name;
	let format = "";
	if( /csv$/i.test(filename) ) {
		format = "csv";
	} else if(/json$/i.test(filename)) {
		format = "json";
	} else {
		alert("file must be csv or json format!");
		return;
	}
	e.dataTransfer.files[0].text().then( (text) =>{ load(text,format)} );
};
div.ondragover = function(e) { e.preventDefault(); };

